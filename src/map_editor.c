#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <ink_data.h>

#include "map_editor.h"
#include "map_saver.h"
#include "map_loader.h"
#include "map.h"
#include "idents.h"
#include "settings.h"
#include "render.h"
#include "prefs.h"

static inkd_listener* listener = 0;
static Map* editing_map = 0;

extern struct ink* ink;


static char* stralloc(const char* src) {
    char* dest = malloc(strlen(src)+1);
    assert(dest);
    strcpy(dest, src);
    return dest;
}

void map_editor_init() {
    if (listener) return;
    listener = inkd_listener_new(0);
    inkd source = ink_source(ink, I_map_editor);
    inkd_add_listener(source, listener);
    load_map_list();
}

void map_editor_shutdown() {
    if (!listener) return;
    inkd_clear_all(ink_source(ink, I_map_editor));
    inkd_listener_delete(listener);
    listener = 0;
}

static void start_editing() {
    inkd source = ink_source(ink, I_editing_map);

    char path[2048];
    strcpy(path, inkd_get_charp(source, I_path));

    const char* name = inkd_get_charp(source, I_name);

    if (inkd_get_bool(source, I_creating)) {
        // if it doesn't exist, create it
        printf("Creating new map.\n");
        Map* m = calloc(1, sizeof(*m));
        m->width = inkd_get_int(source, I_width);
        m->height = inkd_get_int(source, I_height);
        m->tiles = calloc(1, sizeof(*m->tiles) * m->width * m->height);
        m->name = stralloc(name);
        save_map(m);
        destroy_map(m);

        get_path(path, 2048, name);
        inkd_charp(source, I_path, path);
    }
    printf("The path of map %s is %s\n", name, path);
    GameState* gs = start_game(path, GAME_TYPE_MAP_EDITOR);
    editing_map = gs->map;
}

static void end_editing() {
    //assert(editing_map);
    editing_map = 0;
    end_game();
}

static bool valid_tile(Map* map, int x, int y) {
    bool out_of_bounds = (x < 0 || x >= map->width || y < 0 || y >= map->height);
    return !out_of_bounds;
}


static bool handle_mouse(int mouse_x, int mouse_y) {
    if (editing_map == 0) return false;
    inkd source = ink_source(ink, I_map_editor);
    int placing_type = inkd_get_int(source, I_placing_type);

    int x, y;
    map_screen_to_tile(mouse_x, mouse_y, &x, &y);

    if (!valid_tile(editing_map, x,y)) return false;

    if (placing_type < MAP_TILE_TYPE_END) {
        editing_map->tiles[y * editing_map->width + x] = placing_type;
    } else {
        printf("WARNING: Unexpected placing type %d\n", placing_type);
    }
    return true;
}

bool map_editor_event(SDL_Event* ev) {
    if (!listener) return false;

    switch (ev->type) {
        case SDL_MOUSEBUTTONDOWN:
            return handle_mouse(ev->button.x, ev->button.y);
        case SDL_MOUSEMOTION:
            if ((ev->motion.state & SDL_BUTTON_LMASK) || (ev->motion.state & SDL_BUTTON_RMASK)) {
                return handle_mouse(ev->motion.x, ev->motion.y);
            }
            return false;
        default:
            return false;
    }
}

void map_editor_step() {
    if (!listener) return;

    struct ink_data_event ev;
    while (inkd_listener_poll(listener, &ev)) {
        switch (ev.type) {
            case INK_DATA_EVENT_IMPULSE:
                switch (ev.field) {
                    case I_save:
                        if (editing_map) {
                            inkd source = ink_source(ink, I_editing_map);
                            free(editing_map->name);
                            editing_map->name = stralloc(inkd_get_charp(source, I_name));
                            printf("Saving map %s\n", editing_map->name);
                            save_map(editing_map);
                        }
                        break;
                    default:
                        break;
                }
                break;
            case INK_DATA_EVENT_CHANGE:
                switch(ev.field) {
                    case I_editing:
                        if (inkd_get_bool(ev.data, ev.field)) {
                            start_editing();
                        } else {
                            end_editing();
                        }
                        break;
                    default:
                        break;
                }
            default:
                break;
        }
    }
}
