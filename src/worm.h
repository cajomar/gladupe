#ifndef __GLADUPE_WORM__
#define __GLADUPE_WORM__

#include "game.h"

bool any_worm_at(GameState* gs, pathint pos);
enum direction opposite_direction(enum direction d);
pathint apply_direction(const Map* m, enum direction d, pathint loc);
void worm_restart(GameState* gs, npint id);
void worm_add_growth(Worm* w, int ammount);
void worm_multiply_growth(Worm* w, float ammount);
void worm_add_points(Worm* w, int ammount);
void worm_move(Worm* w, const Map* m);
#endif
