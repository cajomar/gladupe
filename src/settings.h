#ifndef __GLADUPE_SETTINGS__
#define __GLADUPE_SETTINGS__

#include <SDL2/SDL.h>

#include "game.h"

enum control_type {
    ControlTypeNone=0,
    ControlTypePlayer,
    ControlTypeComputer,
};

typedef struct worm_settings {
    SDL_Keycode controls[4];
    bool is_human;
    char name[32];
    int color;
} WormSettings;

void set_worm_settings(GameState* gs);
void save_settings();
void set_gamestate_settings(GameState* gs);
void settings_init();
void settings_shutdown();
void settings_step();
bool settings_event(SDL_Event* ev);

#endif
