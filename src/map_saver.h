#ifndef __GLADUPE_MAP_SAVER__
#define __GLADUPE_MAP_SAVER__

#include <stdint.h>
#include "map.h"

int get_path(char* path, int max_out, const char* name);
void save_map(Map*);

#endif
