#ifndef GLADUPE_SRC_FORPATH_H_0ZHDXMYO
#define GLADUPE_SRC_FORPATH_H_0ZHDXMYO

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static inline bool
forpath__fast_forwared(const size_t forpath_index,
                       const size_t alen,
                       const char* const str,
                       size_t* const lenp) {

    for (; forpath_index+*lenp<alen && str[forpath_index+*lenp] != ':'; (*lenp)++) {}
    return true;
}

#define forpath(S,SEP) \
    for (size_t forpath_index=0, /* Index of the start of this string */ \
            lenp=0, /* Length of this string */ \
            lens=strlen(S); \
            forpath_index + lenp < lens /* Counter */ \
            && forpath__fast_forwared(forpath_index, lens, (S), &lenp); /* Find next deliminator */\
            forpath_index+=lenp+1, lenp=0)

#endif /* GLADUPE_SRC_FORPATH_H_0ZHDXMYO */
