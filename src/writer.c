#include "writer.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void w_i32(uint8_t** d, int32_t v) {
    uint32_t len;
    memcpy(&len, *d, 4);
    *d = realloc(*d, len+8);
    // add four to comensate for the integer we have at the begining
    memcpy((*d+len+4), &v, 4);
    // increase the len and update the value
    len += 4;
    memcpy(*d, &len, 4);
}

void w_u8(uint8_t** d, uint8_t v) {    
    uint32_t len;
    memcpy(&len, *d, 4); 
    *d = realloc(*d, len+5);
    // add four to comensate for the integer we have at the begining
    *d[len+4] = v;
    len += 1;
    memcpy(*d, &len, 4);
}

void w_str(uint8_t** d, const char* v) {
    int lenstr = strlen(v);
    w_i32(d, lenstr);
    uint32_t len;
    memcpy(&len, *d, 4);
    *d = realloc(*d, len+lenstr+4);
    // add four to comensate for the integer we have at the begining
    memcpy((*d+len+4), v, lenstr);
    len += lenstr;
    memcpy(*d, &len, 4);
}
