#include <stdio.h>

#include <ink.h>
#include <ink_data.h>
#include <lib2d.h>
#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#include "idents.h"
#include "event_layer.h"
#include "game.h"
#include "map.h"
#include "map_editor.h"
#include "lobby.h"
#include "settings.h"
#include "render.h"
#include "prefs.h"
#include <config.h>

struct ink* ink = 0;
int vp[2];

static inkd_listener* main_listener = 0;

static bool running;
static uint32_t frame_start;
static SDL_Window* win;
static inkd source;

static void start_backround_game() {
    char path[MAX_DATA_PATH];
    prefs_get_path_to_data_file(path, MAX_DATA_PATH, "Main_Menu_Map.gladupe");
    start_game(path, GAME_TYPE_MAIN_MENU);
}

static void end_backround_game() {
    end_game();
}

#ifdef __EMSCRIPTEN__
EM_JS(int, get_canvas_width, (void), { return canvas.width; })
EM_JS(int, get_canvas_height, (void), { return canvas.height; })
EM_JS(void, get_canvas_size, (int* s), {
        s[0] = canvas.width;
        s[1] = canvas.height;
})
#endif /* __EMSCRIPTEN__ */

static void update_screen_size(int w, int h) {
    vp[0] = w;
    vp[1] = h;
    lib2d_viewport(w, h);
    ink_setViewport(ink, w, h);
}

static void step(float dt) {
    struct ink_data_event ev;
    while (inkd_listener_poll(main_listener, &ev)) {
        switch (ev.type) {
            case INK_DATA_EVENT_IMPULSE:
                if (ev.field == I_quit) {
                    running = false;
                }
                break;
            case INK_DATA_EVENT_CHANGE:
                if (ev.field == I_lobby) {
                    if (inkd_get_bool(ev.data, ev.field)) {
                        end_backround_game();
                        lobby_init();
                    } else {
                        lobby_shutdown();
                        start_backround_game();
                    }
                } else if (ev.field == I_map_editor) {
                    if (inkd_get_bool(ev.data, ev.field)) {
                        end_backround_game();
                        map_editor_init();
                    } else {
                        map_editor_shutdown();
                        start_backround_game();
                    }
                }
                break;
            default:
                break;
        }
    }
    map_editor_step();
    settings_step();
    lobby_step();
    game_step(dt);
}

void loop() {
    uint32_t now = SDL_GetTicks();
    uint32_t dt = now - frame_start;
    if (dt < 1) {
        SDL_Delay(1 - dt);
        dt += 1-dt;
    }
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                running = false;
                break;
            case SDL_WINDOWEVENT:
                if (event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
                    update_screen_size(event.window.data1, event.window.data2);
                }
                break;
            case SDL_KEYDOWN:
                if (event.key.keysym.sym == SDLK_ESCAPE) {
                    running = false;
                }
                break;
        }
        if (!handle_ink_events(ink, event)) {
            if (game_event(&event)) continue;
            if (map_editor_event(&event)) continue;
            if (settings_event(&event)) continue;
        }
    }

    step(dt);
    ink_step(ink, dt*0.001f);

    if (!inkd_get_bool(source, I_map_editor) && !inkd_get_bool(source, I_lobby)) {
        lib2d_clear(216.f/255.f, 248.f/255.f, 236.f/255.f, 1.f);
    } else {
        lib2d_clear(1.f, 1.f, 1.f, 1.f);
    }
    if ((inkd_get_bool(source, I_lobby) &&
                !inkd_get_bool(ink_source(ink, I_lobby), I_in_game)) ||
            (inkd_get_bool(source, I_map_editor) &&
                !inkd_get_bool(ink_source(ink, I_map_editor), I_editing))) {

        // Flip it so the map preview will render on top of the gui
        ink_render(ink);
        lib2d_render();
    } else {
        lib2d_render();
        ink_render(ink);
    }
    SDL_GL_SwapWindow(win);

    frame_start = now;

#ifdef __EMSCRIPTEN__
    if (!running) emscripten_cancel_main_loop();
#endif
}

int main() {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "SDL_Init failed: %s\n", SDL_GetError());
        return -1;
    }
#ifdef __EMSCRIPTEN__
    get_canvas_size(vp);
#else
    SDL_DisplayMode DM;
    if (SDL_GetCurrentDisplayMode(0, &DM) == 0) {
        vp[0] = DM.w;
        vp[1] = DM.h;
    }
#endif /* __EMSCRIPTEN__ */

#if GLES
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
#else
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
#endif
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    int window_flags = SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE;

    win = SDL_CreateWindow("Gladupe",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            vp[0],
            vp[1],
            window_flags);

    if (win == 0) {
        fprintf(stderr, "SDL_GL_CreateWindow failed: %s\n", SDL_GetError());
        return -1;
    }

    SDL_GLContext ctx = SDL_GL_CreateContext(win);
    if (ctx == 0) {
        fprintf(stderr, "SDL_GL_CreateContext failed: %s\n", SDL_GetError());
        return -1;
    }
    SDL_GL_MakeCurrent(win, ctx);
    SDL_GL_SetSwapInterval(1);

    if (lib2d_init(LIB2D_RENDER_BACKEND_GL, 0) != 0) {
        fprintf(stderr, "lib2d_init failed: %s\n", lib2d_get_error());
        return -1;
    }
    lib2d_viewport(vp[0], vp[1]);

    ink = ink_new(0);
    main_listener = inkd_listener_new(0);
    source = ink_source(ink, I_main);
    inkd_add_listener(source, main_listener);

    if (ink_init_renderer(ink, INK_RENDERER_API_GL, NULL) != 0) {
        fprintf(stderr, "ink renderer failed.\n");
        return -1;
    }
    char path[MAX_DATA_PATH];
    if (prefs_get_path_to_data_file(path, MAX_DATA_PATH, "gui_assets.bin") != 0) {
        fprintf(stderr, "Couldn't find gui_assets.bin\n");
        return -1;
    }
    ink_load_assets_bin(ink, path);

    if (prefs_get_path_to_data_file(path, MAX_DATA_PATH, "gui.bin") != 0) {
        fprintf(stderr, "Couldn't find gui.bin\n");
        return -1;
    }
    ink_load_gui_bin(ink, path);

#ifndef __EMSCRIPTEN__
    ink_net_listen(ink, "ipc://gui/runner.ipc");
#endif /* __EMSCRIPTEN__ */

    ink_instantiate_main_template(ink);
    ink_setViewport(ink, vp[0], vp[1]);

    frame_start = 0;
    running = true;

    render_init();
    start_backround_game();

#ifdef __EMSCRIPTEN__
    emscripten_set_main_loop(loop, 0, 1);
#else
    while (running) loop();
#endif

    if (!inkd_get_bool(source, I_map_editor) && !inkd_get_bool(source, I_lobby)) end_backround_game();
    render_shutdown();
    lib2d_shutdown();
    ink_free_static_globals();
    ink_delete(ink);
    SDL_DestroyWindow(win);
    SDL_Quit();
    return 0;
}
