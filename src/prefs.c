#include "prefs.h"

#ifdef __EMSCRIPTEN__
#include <string.h>
#define MAX_PATH 64
#else
#include "cfgpath.h"
#endif

#include "cute_files.h"
#include "forpath.h"

#include <config.h>
#include <stdbool.h>


void adds(char** strp, const char* src) {
    size_t len = strlen(src);
    memcpy(*strp, src, len);
    (*strp) += len;
    **strp = '\0';
}

void addns(char** strp, const char* src, size_t len) {
    memcpy(*strp, src, len);
    (*strp) += len;
    **strp = '\0';
}

const char* prefs_data_dir(void) {
    static char* data_dir = 0;
    if (data_dir == 0) {
        char out[MAX_PATH];
        get_user_data_folder(out, MAX_PATH, "gladupe");
        if (out[0] == 0) {
            fprintf(stderr, "Failed to find user data directory.\n");
            return 0;
        } else {
            data_dir = malloc(strlen(out) + 1);
            strcpy(data_dir, out);
        }
    }
    return data_dir;
}

const char* prefs_config_dir(void) {
#ifdef __EMSCRIPTEN__
    return "data/";
#else /* __EMSCRIPTEN__ */
    static char* config_dir = 0;
    if (config_dir == 0) {
        char out[MAX_PATH];
        get_user_config_folder(out, MAX_PATH, "gladupe");
        if (out[0] == 0) {
            fprintf(stderr, "Failed to find user config directory.\n");
            return 0;
        } else {
            config_dir = malloc(strlen(out) + 1);
            strcpy(config_dir, out);
        }
    }
    return config_dir;
#endif /* __EMSCRIPTEN__ */
}

int prefs_dir_file(char* out, size_t max_out, const char* dir, const char* filename) {
    memset(out, 0, max_out);
    if (strlen(dir) + strlen(filename) + 1 > max_out) return -1;
    strcpy(out, dir);
    strcat(out, filename);
    return 0;
}

static int concat(char* out, size_t max_out, const char* s1, const char* s2) {
    const size_t len1 = strlen(s1);
    const size_t len2 = strlen(s2);
    if (len1 + len2 >= max_out) {
        return -1;
    }
    memcpy(out, s1, len1);
    memcpy(out + len1, s2, len2 + 1);
    out[len1+len2] = '\0';
    return 0;
}

#define CHECK_PATH(P) \
    if (concat(out, max_out, (P), filename) != 0) { \
        fprintf(stderr, "%lu is too small.\n", max_out); \
        out[0] = '\0'; \
        return -1; \
    } \
    if (cf_file_exists(out)) { \
        printf("Found %s at %s\n", filename, out); \
        return 0; \
    }

int prefs_get_path_to_data_file(char* out, size_t max_out, const char* filename) {
    // Check ./data
    CHECK_PATH("data/")
    // Check local data dir
    CHECK_PATH(prefs_data_dir())
    // Check system-wide data directories
    // TODO make this work on places besides linux
    const char* const dd = getenv("XDG_DATA_DIRS");
    if (!dd) {
        fputs("Couldn't get the value for $XDG_DATA_DIRS environment variable", stderr);
        return -1;
    }

    const char* const gladupe = "gladupe";
    size_t glen = strlen(gladupe);

    forpath(dd, ':') {
        if (lenp + 1 + glen + 1 + strlen(filename) + 1 > max_out) {
            printf("%lu is too small.\n", max_out);
            continue;
        }
        char* s = out;
        addns(&s, dd+forpath_index, lenp);
        *(s++) = '/';
        addns(&s, gladupe, glen);
        *(s++) = '/';
        adds(&s, filename);

        if (cf_file_exists(out)) {
            printf("Found %s at %s\n", filename, out);
            return 0;
        }
    }

    fprintf(stderr, "Couldn't find %s in %s, %s, or XDG_DATA_DIRS\n", filename, "data/", prefs_data_dir());
    return -1;
}
#define CHECK_PATH_FP(P) \
    if (concat(p, MAX_PATH, (P), filename) != 0) { \
        fprintf(stderr, "%d is too small.\n", MAX_PATH); \
        return 0; \
    } \
    fp = fopen(p, "rb"); \
    if (fp) return fp;

FILE* prefs_get_config_fp(const char* filename) {
    FILE* fp;
    char p[MAX_PATH];
    // Check ./data
    CHECK_PATH_FP("data/")
    // Check local data dir
    CHECK_PATH_FP(prefs_config_dir())
    return 0;
}
