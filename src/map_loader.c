#include <stdlib.h>
#include <unistd.h>

#include <ink_query.h>

#define CUTE_FILES_IMPLEMENTATION
#include "cute_files.h"
#include "map_loader.h"
#include "prefs.h"
#include "reader.h"
#include "idents.h"
#include "forpath.h"
#include <config.h>

extern struct ink* ink;

static void error(const char* str) {
    fprintf(stderr, "%s\n", str);
    exit(EXIT_FAILURE);
}

Map* get_default_map() {
    Map* m = malloc(sizeof(Map));
    if (!m) error("malloc() failed when creating map");
    m->name = strdup("Unloadable");
    if (!m) error("strdup() failed when copying map name");

    m->width = 30;
    m->height = 20;
    m->tiles = malloc(sizeof(*m->tiles) * m->width * m->height);
    if (!m) error("malloc() failed when creating map");
    for (int i=0; i<m->width*m->height; i++) {
        m->tiles[i] = MAP_TILE_TYPE_EMPTY;
    }
    return m;
}

Map* load_map(const char* path) {
    FILE* fp = fopen(path, "rb");
    if (fp == NULL) {
        fprintf(stderr, "Failed to open map file: %s\n", path);
        return NULL;
    }
    fseek(fp, 0, SEEK_END);
    int count = ftell(fp);
    rewind(fp);
    uint8_t* buffer = malloc(count);
    fread(buffer, count, 1, fp);
    fclose(fp);

    uint8_t* r = buffer;
    Map* m = map_loader_deserialize(&r);
    free(buffer);
    return m;
}

// TODO make sure this function doesn't read past the buffer
Map* map_loader_deserialize(uint8_t** r) {
    Map* m = malloc(sizeof(Map));

    if (!m) {
        fputs("malloc() failed when loading map\n", stderr);
        return NULL;
    }

    m->name = r_str(r);
    if (!m->name) {
        fputs("Error loading map: Failed to load map name\n", stderr);
        goto failed;
    }

    m->width = r_i32(r);

    if (m->width <= 0) {
        fprintf(stderr, "Error loading map: Invalid map width %d\n", m->width);
        goto failed;
    }

    m->height = r_i32(r);

    if (m->height <= 0) {
        fprintf(stderr, "Error loading map: Invalid map height %d\n", m->height);
        goto failed;
    }

    m->tiles = malloc(sizeof(*m->tiles) * m->width * m->height);
    if (!m->tiles) {
        fputs("malloc() failed when creating map tiles\n", stderr);
        goto tiles_failed;
    }
    for (int i=0; i<m->width*m->height; i++) {
        m->tiles[i] = r_i32(r);
    }

    return m;

tiles_failed:
    free(m->tiles);
failed:
    free(m->name);
    free(m);
    return NULL;
}


int load_map_metadata(const char* path, char* name, int string_max) {
    memset(name, 0, string_max);
    FILE* fp = fopen(path, "rb");
    if (fp == NULL) {
        fprintf(stderr, "Failed to open file while loading map metadata: %s\n", path);
        return -1;
    }

    int name_len = fp_i32(fp);
    if (name_len > string_max-1) {
        fprintf(stderr, "Map name too long: %d\n", name_len);
        fclose(fp);
        return -1;
    }
    fread(name, 1, name_len, fp);

    fclose(fp);
    return 0;
}

static bool ends_with(const char* name, const char* suffix) {
    int n = strlen(name);
    int s = strlen(suffix);
    if (n < s) return false;
    return strcmp(suffix, name + (n-s)) == 0;
}

static int file_in_dir(char* out, size_t max_out, const char* dirname, const char* filename) {
    memset(out, 0, max_out);
    if (strlen(dirname) + strlen(filename) + 1 > max_out) return -1;
    strcpy(out, dirname);
    strcat(out, filename);
    return 0;
}

static void load_maps_from_dir(const char* dirname, inkd source, ink_query* q) {
    if (!dirname) return;
    printf("Loading maps from %s\n", dirname);
    if (access(dirname, R_OK) != 0) return;
    cf_dir_t dir;
    cf_dir_open(&dir, dirname);
    while (dir.has_next) {
        cf_file_t file;
        cf_read_file(&dir, &file);
        if (ends_with(file.name, ".gladupe")) {
            char path[2048];
            if (file_in_dir(path, 2048, dirname, file.name) == 0) {
                char name[256];
                if (load_map_metadata(path, name, 256) == 0) {
                    ink_query_charp(q, I_name, name);
                    inkd d = ink_query_get(q, source, I_maps);
                    inkd_charp(d, I_name, name);
                    inkd_charp(d, I_path, path);

                    Map* m = load_map(path);
                    if (!m) continue;
                    inkd_int(d, I_width, m->width);
                    inkd_int(d, I_height, m->height);
                    destroy_map(m);
                }
            }
        }
        cf_dir_next(&dir);
    }
    cf_dir_close(&dir);
}

void load_map_list(void) {
    inkd source = ink_source(ink, I_map_selection);
    inkd_rebuild_begin(source, I_maps);
    ink_query q;
    ink_query_init(&q);

    load_maps_from_dir("data/", source, &q);
    load_maps_from_dir(prefs_data_dir(), source, &q);
    const char* const dd = getenv("XDG_DATA_DIRS");
    if (dd) {
        forpath(dd, ':') {
            const char* gladupe = "gladupe";
            size_t leng = strlen(gladupe);
            char p[lenp + 1 + leng + 1 + 1];
            char* s = p;

            addns(&s, dd+forpath_index, lenp);
            *(s++) = '/';
            addns(&s, gladupe, leng);
            *(s++) = '/';
            *(s++) = '\0';

            load_maps_from_dir(p, source, &q);
        }
    } else {
        fprintf(stderr, "Couldn't get value of XDG_DATA_DIRS envionment variable.\n");
    }

    ink_query_deinit(&q);
    inkd_rebuild_end(source, I_maps);
}
