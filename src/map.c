#include "map.h"

#include <assert.h>
#include <stdlib.h>

int get_shuffled_index(int total, int jump_factor, int i) {
    int new_i = i * jump_factor;
    while (new_i >= total)
        new_i += 1 - total;
    assert(new_i < total);
    return new_i;
}

void wrap(Map* m, int* x, int* y) {
    while (*x < 0) *x = m->width + (*x);
    while (*x >= m->width) *x = (*x) - m->width;
    while (*y < 0) *y = m->height + (*y);
    while (*y >= m->height) *y = (*y) - m->height;
}

pathint flatten(const Map* map, int x, int y) {
    return ((y+map->height)%map->height) * map->width + ((x+map->width)%map->width);
}

void destroy_map(Map* m) {
    assert(m);
    free(m->name);
    free(m->tiles);
    free(m);
}
