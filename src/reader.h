#ifndef __GLADUPE_READER__
#define __GLADUPE_READER__
#include <stdint.h>
#include <stdio.h>

int32_t r_i32(uint8_t**);
uint8_t r_u8(uint8_t**);
char* r_str(uint8_t**);

int32_t fp_i32(FILE* fp);
uint8_t fp_u8(FILE* fp);
char* fp_str(FILE* fp);

#endif
