#ifndef __GLADUPE_MAP_LOADER__
#define __GLADUPE_MAP_LOADER__

#include "map.h"
#include <ink_data.h>

Map* get_default_map(void);
Map* map_loader_deserialize(uint8_t**);
Map* load_map(const char* path);
int load_map_metadata(const char* path, char* name, int string_max);
void load_map_list(void);

#endif
