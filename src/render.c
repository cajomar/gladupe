#include "render.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>

#include <lib2d.h>

#include "food.h"
#include "idents.h"
#include "queue.h"
#include "stretchy_buffer.h"
#include "prefs.h"
#include <config.h>

#define lib2d_image_delete(i) ((i) ? lib2d_image_delete(i) : (void)0)

#define GOOP_VARIATIONS 3
#define GOOP_SHINE_VARIATIONS 4
#define GRASS_VARIATIONS 3

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

extern struct ink* ink;
static bool initalized = false;
extern int vp[2];

// Number of milliseconds since the game started.
// Used to bounce food.
static unsigned int time;

static lib2d_drawable drawable;
static lib2d_drawable eye;
static lib2d_drawable hole;
static lib2d_drawable food_sprites[FOOD_TYPE_END];
static lib2d_drawable food_sprite_shadows[FOOD_TYPE_END];
static lib2d_drawable goop[GOOP_VARIATIONS];
static lib2d_drawable goop_shine[GOOP_SHINE_VARIATIONS];
static lib2d_drawable grass[GRASS_VARIATIONS];

int x_padding = 0;
const int y_padding = 64;

int tile_size = 0;

// x, y, w, h
static const float goop_shine_offsets[GOOP_SHINE_VARIATIONS * 4] = {
    0.1328125f, 0.1250000f, 0.3984375f, 0.2890625f,
    0.6953125f, 0.1171875f, 0.1718750f, 0.2031250f,
    0.6796875f, 0.4140625f, 0.1875000f, 0.1875000f,
    0.1484375f, 0.4062500f, 0.1562500f, 0.2031250f,
};

static const int food_sprite_animation_framerate[FOOD_TYPE_END] = {
    300, // FOOD_TYPE_APPLE
    300, // FOOD_TYPE_BANANA
    300, // FOOD_TYPE_CHERRY
    200, // FOOD_TYPE_DOUGHNUT
    300, // FOOD_TYPE_DIMOND
    300, // FOOD_TYPE_HEART
    100, // FOOD_TYPE_CLOCK_FAST
    800, // FOOD_TYPE_CLOCK_SLOW
};


static void calculate_render_dimensions(Map* map) {
    int w = vp[0]/map->width;
    int h = (vp[1]-y_padding)/map->height;
    tile_size = w < h ? w:h;

    x_padding = vp[0]/2 - (map->width*tile_size)/2;
}

static void calculate_render_dimensions_map_preview(Map* map) {
    tile_size = 360/map->height;

    // centered
    //x_padding = inkd_get_int(ink_source(ink, I_lobby), I_preview_x) - map->width*tile_size/2 - 48;

    // not centered
    x_padding = inkd_get_int(ink_source(ink, I_map_selection), I_preview_x) - 32;
}

void map_tile_to_screen(int x, int y, float* px, float* py) {
    *px = x*tile_size + x_padding;
    *py = y*tile_size + y_padding;
}

void map_screen_to_tile(int px, int py, int* x, int* y) {
    *x = (px-x_padding)/tile_size;
    *y = (py-y_padding)/tile_size;
}

float animate(int dt, int total_animation_length, enum overtime_anim_schemes scheme) {
    int new_dt;
    switch (scheme) {
        case ANIM_STOP:
            if (dt > total_animation_length) {
                new_dt = total_animation_length;
            } else if (dt < 0) {
                new_dt = 0;
            } else {
                new_dt = dt;
            }
            break;
        case ANIM_CONTINUE:
            new_dt = dt;
            break;
        case ANIM_BOUNCE:
            new_dt = dt % (total_animation_length * 2);
            if (new_dt > total_animation_length) new_dt = total_animation_length*2 - new_dt;
            break;
        case ANIM_JUMP:
            new_dt = dt % total_animation_length;
            break;
        default:
            printf("WARNING: Unknown overtime animation scheme %d\n", scheme);
            new_dt = dt;
            break;
    }
    return (float) new_dt/total_animation_length;
}

static void load_sprite_image(const char* path, lib2d_drawable* d) {
    lib2d_image_info info;
    d->image = lib2d_image_load(path, &info);
    if (!d->image) {
        fprintf(stderr, "Error loading %s: %s\n", path, lib2d_get_error());
    }
    d->x = 0.f;
    d->y = 0.f;
    d->w = info.w;
    d->h = info.h;
    d->rot = 0.f;
    d->color = 0xffffffff;
}

void render_init() {
    if (initalized) assert(false);
    initalized = true;

    const char* food_names[FOOD_TYPE_END] = {
        "apple",
        "banana",
        "cherry",
        "doughnut",
        "dimond",
        "heart",
        "clock_fast",
        "clock_slow"
    };
    char path[MAX_DATA_PATH];
    prefs_get_path_to_data_file(path, MAX_DATA_PATH, "eye.png");
    load_sprite_image(path, &eye);
    prefs_get_path_to_data_file(path, MAX_DATA_PATH, "hole.png");
    load_sprite_image(path, &hole);

    char p[128];
    for (int i=0; i<FOOD_TYPE_END; i++) {
        sprintf(p, "%s.png", food_names[i]);
        prefs_get_path_to_data_file(path, MAX_DATA_PATH, p);
        load_sprite_image(path, &food_sprites[i]);

        sprintf(p, "%s_shadow.png", food_names[i]);
        prefs_get_path_to_data_file(path, MAX_DATA_PATH, p);
        load_sprite_image(path, &food_sprite_shadows[i]);
    }
    for (int i=0; i<GOOP_VARIATIONS; i++) {
        sprintf(p, "goop%d.png", i+1);
        prefs_get_path_to_data_file(path, MAX_DATA_PATH, p);
        load_sprite_image(path, &goop[i]);
    }
    for (int i=0; i<GOOP_SHINE_VARIATIONS; i++) {
        sprintf(p, "goopshine%d.png", i+1);
        prefs_get_path_to_data_file(path, MAX_DATA_PATH, p);
        load_sprite_image(path, &goop_shine[i]);
    }
    for (int i=0; i<GRASS_VARIATIONS; i++) {
        sprintf(p, "grass%d.png", i+1);
        prefs_get_path_to_data_file(path, MAX_DATA_PATH, p);
        load_sprite_image(path, &grass[i]);
    }
    time = 0;
}

void render_shutdown() {
    assert(initalized);
    initalized = false;
    for (int i=0; i<FOOD_TYPE_END; i++) {
        lib2d_image_delete(food_sprites[i].image);
        lib2d_image_delete(food_sprite_shadows[i].image);
    }
    for (int i=0; i<GOOP_VARIATIONS; i++) {
        lib2d_image_delete(goop[i].image);
    }
    for (int i=0; i<GOOP_SHINE_VARIATIONS; i++) {
        lib2d_image_delete(goop_shine[i].image);
    }
    for (int i=0; i<GRASS_VARIATIONS; i++) {
        lib2d_image_delete(grass[i].image);
    }
    lib2d_image_delete(eye.image);
    lib2d_image_delete(hole.image);
}

static void render_backround(const Map* map) {
    assert(initalized);
    drawable.w = tile_size*map->width;
    drawable.h = tile_size*map->height;
    map_tile_to_screen(0, 0, &drawable.x, &drawable.y);
    drawable.color = 0xd8f8ecff;
    lib2d_draw(&drawable);
}

static void draw_wall(const Map* map, int x, int y) {
    // draw border
    drawable.w = tile_size;
    drawable.h = tile_size;
    drawable.color = 0x515151ff;
    map_tile_to_screen(x, y, &drawable.x, &drawable.y);
    lib2d_draw(&drawable);

    // draw top
    drawable.color = 0xccccccff;
    if (map->tiles[flatten(map, x-1, y)] != MAP_TILE_TYPE_WALL) {
        drawable.x += tile_size/16;
        drawable.w -= tile_size/16;
    }
    if (map->tiles[flatten(map, x+1, y)] != MAP_TILE_TYPE_WALL) {
        drawable.w -= tile_size/16;
    }
    if (map->tiles[flatten(map, x, y-1)] != MAP_TILE_TYPE_WALL) {
        drawable.y += tile_size/16;
        drawable.h -= tile_size/16;
    }
    if (map->tiles[flatten(map, x, y+1)] != MAP_TILE_TYPE_WALL) {
        drawable.h -=tile_size/16;
    }
    lib2d_draw(&drawable);

    // draw side
    if (map->tiles[flatten(map, x, y+1)] != MAP_TILE_TYPE_WALL) {
        drawable.color = 0xb3b3b3ff;
        drawable.y += tile_size*2/3;
        drawable.h -= tile_size*2/3;
        lib2d_draw(&drawable);

        // draw bricks
        if (tile_size > 16) {
            drawable.color = 0x999999ff;
            // save the side values
            float brick_width = tile_size/5;
            float brick_height = tile_size/15;
            float mortar = brick_width/4;
            float sh = drawable.h-brick_height;
            float sw = drawable.w;
            float sx = drawable.x;
            float sy = drawable.y+brick_height;
            drawable.w = brick_width;
            drawable.h = brick_height;

            for (int i=0; i<3; i++) {
                int rand = x*y+i;
                int nh = sh/(brick_height+mortar);
                if (nh<=0) nh += 1-nh;
                int nw = sw/(brick_width+mortar);

                int by = rand % nh;

                drawable.y = sy + by * (brick_height+mortar);
                drawable.x = sx + (rand%nw) * (brick_width+mortar);
                // offset for brickiness
                if (by/2 == by%2) drawable.x += (brick_width+mortar)/2;

                lib2d_draw(&drawable);
            }
        }
    }
}

static void render_obstacles(const Map* map) {
    assert(initalized);
    assert(map);
    for (int y=0; y<map->height; y++) {
        for (int x=0; x<map->width; x++) {
            switch (map->tiles[flatten(map, x, y)]) {
                case MAP_TILE_TYPE_WALL:
                    draw_wall(map, x, y);
                    break;
                case MAP_TILE_TYPE_HOLE:
                    hole.w = tile_size;
                    hole.h = tile_size;
                    hole.color = 0xffffffff;
                    map_tile_to_screen(x, y, &hole.x, &hole.y);
                    lib2d_draw(&hole);
                    break;
                default:
                    break;
            }
        }
    }
}

static void draw_goop(GameState* gs, int time_left, float scale, int from, int to, int* x, int* y) {
    int to_x = to % gs->map->width;
    int to_y = to / gs->map->width;
    int from_x = from % gs->map->width;
    int from_y = from / gs->map->width;
    int v = gs->noise[to] % GOOP_VARIATIONS;
    lib2d_drawable* g = &goop[v];
    map_tile_to_screen(from_x, from_y, &g->x, &g->y);

    g->rot = 0.f;

    g->w = tile_size * scale;
    g->h = tile_size * scale;

    g->x += (tile_size - tile_size*scale)/2;
    g->y += (tile_size - tile_size*scale)/2;

    int dx = abs(to_x - from_x);
    int dy = abs(to_y - from_y);

    if (dx == 1 && dy == 0) {
        g->x += tile_size*animate(gs->framerate-time_left, gs->framerate, ANIM_STOP)*(to_x-from_x);
    }
    if (dx == 0 && dy == 1) {
        g->y += tile_size*animate(gs->framerate-time_left, gs->framerate, ANIM_STOP)*(to_y-from_y);
    }

    lib2d_draw(g);

    *x = g->x;
    *y = g->y;
}

void render(GameState* gs, int dt, int time_left) {
    if (!initalized) return;

    if (gs->type == GAME_TYPE_MAP_PREVIEW) {
        calculate_render_dimensions_map_preview(gs->map);
    } else {
        calculate_render_dimensions(gs->map);
    }

    time += dt;

    if (gs->type != GAME_TYPE_MAIN_MENU)
        render_backround(gs->map);

    render_obstacles(gs->map);

    //  For debugging
#ifdef RENDER_FOOD_MAP
    drawable.w = tile_size;
    drawable.h = tile_size;
    for (int y = 0; y < gs->map->height; y++) {
        for (int x = 0; x < gs->map->width; x++) {
            map_tile_to_screen(x, y, &drawable.x, &drawable.y);
            int v = gs->food_map[y*gs->map->width + x];
            drawable.color = 0xff00 | MIN(0xff, v*10);
            lib2d_draw(&drawable);
        }
    }
#endif

    for (int i=0; i<GRASS_VARIATIONS; i++) {
        grass[i].w = tile_size*3/2;
        grass[i].h = tile_size*3/2;
    }
    for (int i=0; i<gs->map->width*gs->map->height/100; i ++) {
        // draw grass
        int new_i = get_shuffled_index(gs->map->width*gs->map->height, 100, i);
        new_i += gs->noise[i] % 100;
        if (gs->map->tiles[new_i] != MAP_TILE_TYPE_EMPTY) continue;
        int x = new_i % gs->map->width;
        int y = new_i / gs->map->width;
        map_tile_to_screen(x, y, &grass[new_i % GRASS_VARIATIONS].x, &grass[new_i % GRASS_VARIATIONS].y);
        lib2d_draw(&grass[new_i % GRASS_VARIATIONS]);
    }

    // if the worms and food are hidden, there's no need to go on
    if (gs->type == GAME_TYPE_MAP_EDITOR && !inkd_get_bool(ink_source(ink, I_map_editor), I_show_worms)) return;

    // update sprite dimentions
    for (int i=0; i<FOOD_TYPE_END; i++) {
        food_sprites[i].w = tile_size*2;
        food_sprites[i].h = tile_size*2;

        food_sprite_shadows[i].w = food_sprites[i].w;
        food_sprite_shadows[i].h = food_sprites[i].h/3*2;
    }
    for (int i=0; i<GOOP_SHINE_VARIATIONS; i++) {
        goop_shine[i].w = goop_shine_offsets[i*4+2] * tile_size * 0.9f;
        goop_shine[i].h = goop_shine_offsets[i*4+3] * tile_size * 0.9f;
    }

    sbforeachp(Food* f, gs->food) {
        // draw food shadows
        lib2d_drawable* sprite_shadow = &food_sprite_shadows[f->type];
        map_tile_to_screen(f->x, f->y, &sprite_shadow->x, &sprite_shadow->y);
        sprite_shadow->y += tile_size/3*2;
        sprite_shadow->x -= tile_size/2;
        sprite_shadow->x -= tile_size/4*animate(time, (food_sprite_animation_framerate[f->type]), ANIM_BOUNCE);
        lib2d_draw(sprite_shadow);
        // draw food
        lib2d_drawable* sprite = &food_sprites[f->type];
        map_tile_to_screen(f->x, f->y, &sprite->x, &sprite->y);
        sprite->y -= tile_size/3*animate(time, (food_sprite_animation_framerate[f->type]), ANIM_BOUNCE);
        lib2d_draw(sprite);
    }

    // draw worms
    eye.w = tile_size/4; // TODO move this line?
    eye.h = eye.w;
    sbforeachp(Worm* w, gs->worms) {
        const int wx = queue_last(w->path) % gs->map->width;
        const int wy = queue_last(w->path) / gs->map->width;
        const int nx = queue_getv(w->path, -2) % gs->map->width;
        const int ny = queue_getv(w->path, -2) / gs->map->width;
        if (abs(wx - nx) == 1) {
            inkd_int(w->data, I_x, x_padding + \
                    tile_size * nx + \
                    tile_size * animate(gs->framerate-time_left, gs->framerate, ANIM_CONTINUE) * (wx - nx));
        } else {
            inkd_int(w->data, I_x, x_padding + tile_size * nx);
        }
        if (abs(wy - ny) == 1) {
            inkd_int(w->data, I_y, y_padding + \
                    tile_size * ny + \
                    tile_size * animate(gs->framerate-time_left, gs->framerate, ANIM_CONTINUE) * (wy - ny));
        } else {
            inkd_int(w->data, I_y, y_padding + tile_size * ny);
        }

        for (int i=0; i<GOOP_VARIATIONS; i++) {
            goop[i].color = w->color;
        }
        // Indicates which segment to stop animating the worm segments.
        // If < 0, animate everything.
        int draw_stationary = -1;

        // cx and cy are where the the segment we're drawing is currently at
        // (since the last game step), px and py are where it was in the game
        // step before last.

        // store where we drew the segment so we can draw the goop shine
        int gsx, gsy;
        // loop through all the worm's segments, starting at the head
        for (int i=queue_count(w->path)-1; i>=0; --i) {
            int c = queue_getv(w->path, i);
            int p;
            if (i) {
                p = queue_getv(w->path, i-1);
            } else {
                p = w->tail;
            }

            if (queue_getv(w->events, i) == WORM_EVENT_EAT &&
                    w->growth > 0 &&
                    (draw_stationary < 0 || i - draw_stationary < w->growth)) {
                // draw enlarged goop if the worm ate something here and is
                // growing from it
                draw_goop(gs, time_left, 1.1f, i > 0 ? c : p, c, &gsx, &gsy);

                if (draw_stationary < 0) {
                    draw_stationary = i;
                }

            } else if (draw_stationary >= 0 || time_left > gs->framerate || gs->framerate <= 30) {
                // draw stationary if the worm is growing, if we're in a
                // countdown, or if the framerate is too fast
                draw_goop(gs, 0, 0.9f, c, c, &gsx, &gsy);
            } else {
                // draw moving
                draw_goop(gs, time_left, 0.9f, p, c, &gsx, &gsy);
            }
            if (i % 5 == 0 && i < queue_count(w->path) - 1) {
                int s = (i) % GOOP_SHINE_VARIATIONS;
                lib2d_drawable* g = &goop_shine[s];
                g->x = gsx + goop_shine_offsets[s*4+0]*tile_size;
                g->y = gsy + goop_shine_offsets[s*4+1]*tile_size;
                lib2d_draw(g);
            }
        }

        // draw eyes
        eye.rot = -2.f * M_PI * animate(time, 2000.f, ANIM_CONTINUE);
        switch(w->direction) {
            case DIRECTION_NORTH:
                map_tile_to_screen(wx, wy, &eye.x, &eye.y);
                eye.x += eye.w/2 + tile_size/8;
                eye.y += tile_size/4;
                if (time_left <= gs->framerate) {
                    eye.y -= tile_size * animate(gs->framerate-time_left,
                            gs->framerate, ANIM_STOP);
                    eye.y += tile_size;
                }
                lib2d_draw(&eye);
                eye.x += tile_size/4;
                eye.rot = 2 * M_PI * animate(time, 3300, ANIM_CONTINUE);
                lib2d_draw(&eye);
                break;
            case DIRECTION_EAST:
                map_tile_to_screen(wx, wy, &eye.x, &eye.y);
                eye.y += eye.w/2;
                eye.x += tile_size/2;
                if (time_left <= gs->framerate) {
                    eye.x += tile_size * animate(gs->framerate-time_left,
                            gs->framerate, ANIM_STOP);
                    eye.x -= tile_size;
                }
                lib2d_draw(&eye);
                eye.y += tile_size/4;
                eye.rot = 2 * M_PI * animate(time, 3300, ANIM_CONTINUE);
                lib2d_draw(&eye);
                break;
            case DIRECTION_SOUTH:
                map_tile_to_screen(wx, wy, &eye.x, &eye.y);
                eye.x += eye.w/2 + tile_size/8;
                eye.y += tile_size/12*5;
                if (time_left <= gs->framerate) {
                    eye.y += tile_size * animate(gs->framerate-time_left,
                            gs->framerate, ANIM_STOP);
                    eye.y -= tile_size;
                }
                lib2d_draw(&eye);
                eye.x += tile_size/4;
                eye.rot = 2 * M_PI * animate(time, 3300, ANIM_CONTINUE);
                lib2d_draw(&eye);
                break;
            case DIRECTION_WEST:
                map_tile_to_screen(wx, wy, &eye.x, &eye.y);
                eye.y += eye.w/2;
                eye.x += tile_size/4;
                if (tile_size <= gs->framerate) {
                    eye.x -= tile_size * animate(gs->framerate-time_left,
                            gs->framerate, ANIM_STOP);
                    eye.x += tile_size;
                }
                lib2d_draw(&eye);
                eye.y += tile_size/4;
                eye.rot = 2 * M_PI * animate(time, 3300, ANIM_CONTINUE);
                lib2d_draw(&eye);
                break;
            default:
                break;
        }
    }
}
