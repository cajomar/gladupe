#ifndef __GLADUPE_MAP__
#define __GLADUPE_MAP__

#include <stdbool.h>

#include "types.h"

enum map_tile_type {
    MAP_TILE_TYPE_EMPTY=0,
    MAP_TILE_TYPE_WALL,
    MAP_TILE_TYPE_HOLE,
    MAP_TILE_TYPE_END
};

typedef struct map {
    char* name;
    enum map_tile_type* tiles;
    int width;
    int height;
} Map;

pathint flatten(const Map* map, int x, int y);
enum map_tile_type get_map_tile(Map* m, int x, int y);
int get_shuffled_index(int total, int jump_factor, int i);
void wrap(Map* m, int* x, int* y);
void destroy_map(Map* m);

#endif
