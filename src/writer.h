#ifndef __GLADUPE_WRITER__
#define __GLADUPE_WRITER__
#include <stdint.h>



void w_i32(uint8_t** w, int32_t v);
void w_u8(uint8_t** w, uint8_t v);
void w_str(uint8_t** w, const char* v);

#endif
