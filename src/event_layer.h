#ifndef __GLADUPE_EVENT_LAYER__
#define __GLADUPE_EVENT_LAYER__

#include <ink.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>

bool handle_ink_events(struct ink* ink, SDL_Event event);

#endif
