#ifndef __GLADUPE_RENDER__
#define __GLADUPE_RENDER__

#include "game.h"


enum overtime_anim_schemes {
    ANIM_STOP=0,
    ANIM_CONTINUE,
    ANIM_BOUNCE,
    ANIM_JUMP
};

float animate(int dt, int total_animation_length, enum overtime_anim_schemes scheme);
void map_tile_to_screen(int x, int y, float* px, float* py);
void map_screen_to_tile(int px, int py, int* x, int* y);
void render_init();
void render_shutdown();
void render(GameState* gs, int dt, int time_left);

#endif
