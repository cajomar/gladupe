#include "worm.h"

#include <assert.h>
#include <stdio.h>

#include "food.h"
#include "idents.h"
#include "queue.h"
#include "stretchy_buffer.h"


extern struct ink* ink;


enum direction opposite_direction(enum direction d) {
    switch (d) {
        case DIRECTION_NORTH: return DIRECTION_SOUTH;
        case DIRECTION_EAST: return DIRECTION_WEST;
        case DIRECTION_SOUTH: return DIRECTION_NORTH;
        case DIRECTION_WEST: return DIRECTION_EAST;
        default:
            printf("WARNING: Unexpected value %d passed for d in opposite_direction\n", d);
            return DIRECTION_END;
    }
}

static bool cardinal(const Map* m, pathint a, pathint b) {
    int ax = a % m->width;
    int ay = a / m->width;
    int bx = b % m->width;
    int by = b / m->width;
    return ax == bx || ay == by;
}

enum direction get_direction_to(const Map* m, pathint from, pathint to) {
    int fx = from % m->width;
    int fy = from / m->width;
    int tx = to % m->width;
    int ty = to / m->width;

    if (fx == tx) {
        return ty - fy <= m->height ? DIRECTION_SOUTH : DIRECTION_NORTH;
    } else {
        if (fy != ty) {
            fprintf(stderr, "ERROR: fy is %d and ty is %d\n", fy, ty);
            return DIRECTION_END;
        }
        return tx - fx <= m->width ? DIRECTION_WEST : DIRECTION_EAST;
    }
}

pathint apply_direction(const Map* m, enum direction d, pathint loc) {
    /* Returns the tile index to the d of loc. map is used to wrap it. */
    int x = loc % m->width;
    int y = loc / m->width;
    switch (d) {
    case DIRECTION_NORTH:
        loc = flatten(m, x, y-1);
        break;
    case DIRECTION_EAST:
        loc = flatten(m, x+1, y);
        break;
    case DIRECTION_SOUTH:
        loc = flatten(m, x, y+1);
        break;
    case DIRECTION_WEST:
        loc = flatten(m, x-1, y);
        break;
    default:
        printf("WARNING: %d is not a direction!\n", d);
        break;
    }
    return loc;
}

static bool worm_is_at(Worm* w, pathint pos) {
    for (pathint i=0; i<queue_count(w->path); i++) {
        int p = queue_getv(w->path, i);
        if (p == pos) return true;
    }
    return false;
}

bool any_worm_at(GameState* gs, pathint pos) {
    sbforeachp(Worm* w, gs->worms) {
        if (worm_is_at(w, pos)) return true;
    }
    return false;
}

void worm_restart(GameState* gs, npint id) {
    Worm* w = &gs->worms[id];
    queue_clear(w->path);
    queue_clear(w->events);
    queue_clear(w->direction_queue);
    w->growth = 3;
    w->reverse = false;
    // Get a starting location for the worm
    // loop through every possible starting spot
    bool found = false;
    for (long i=0; i<gs->map->height*gs->map->width && !found; i++) {
        const pathint pos = get_shuffled_index(gs->map->width*gs->map->height, gs->map->width*2.2, i);
        // check if the tile is occupied
        if (gs->map->tiles[pos] != MAP_TILE_TYPE_EMPTY || any_worm_at(gs, pos))
            continue;
        // loop through all the directions
        for (enum direction d=0; d<4 && !found; d++) {
            bool is_good = true;
            // check if this direction has a runway
            pathint loc = pos;
            for (int j=1; j<10 && is_good; j++) {
                loc = apply_direction(gs->map, d, loc);
                if (gs->map->tiles[loc] != MAP_TILE_TYPE_EMPTY) {
                    is_good = false;
                }
                sbforeachp(Worm* o, gs->worms) {
                    if (is_good) break;
                    if (stb__counter == id) continue;
                    if (worm_is_at(o, loc)) is_good = false;
                }
            }
            if (is_good) {
                w->tail = apply_direction(gs->map, opposite_direction(d), pos);
                queue_push(w->path, pos);
                queue_push(w->events, WORM_EVENT_NONE);
                w->direction = d;
                found = true;
            }
        }
    }
    if (!found) {
        printf("WARNING: No suitable starting location for worm %d found in map %s.\n", id, gs->map->name);
        w->tail = 0;
        queue_push(w->path, 0);
        queue_push(w->events, WORM_EVENT_NONE);
        w->direction = DIRECTION_EAST;
    }
}

void worm_add_growth(Worm* w, int ammount) {
    int old_length = queue_count(w->path);

    if (ammount >= 0) {
        w->growth += ammount;
    } else {
        int l = old_length + w->growth + ammount;
        l = MAX(1, MIN(l, old_length));
        while (queue_count(w->path) > l) {
            w->tail = queue_pop(w->path);
            queue_pop(w->events);
        }
        w->growth = 0;
    }
}

void worm_multiply_growth(Worm* w, float ammount) {
    int total = queue_count(w->path) + w->growth;
    int new_total = total*ammount;
    worm_add_growth(w, new_total - total);
}

void worm_add_points(Worm* w, int ammount) {
    w->points += ammount;
    w->time_since_last_ate = 0;
    if (w->data) {
        inkd_int(w->data, I_ammount_changed, ammount);
    }
}

void worm_move(Worm* w, const Map* m) {
    pathint h;
    if (w->reverse) {
        h = w->tail;
        w->tail = apply_direction(m, w->direction, queue_last(w->path));
        queue_reverse(w->path);
        queue_reverse(w->events);
        queue_clear(w->direction_queue);
        w->direction = get_direction_to(m, queue_last(w->path), h);
        if (w->direction == DIRECTION_END) {
            fprintf(stderr, "Error here.\n");
        }
        w->reverse = false;
    } else {
        h = apply_direction(m, w->direction, queue_last(w->path));
    }
    queue_push(w->path, h);
    queue_push(w->events, WORM_EVENT_NONE);

    if (w->growth > 0) {
        w->growth--;
    } else {
        w->tail = queue_pop(w->path);
        queue_pop(w->events);
    }
    assert(queue_count(w->path) == queue_count(w->events));
}
