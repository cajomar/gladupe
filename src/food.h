#ifndef __GLADUPE_FOOD__
#define __GLADUPE_FOOD__

#include "game.h"

void add_food(GameState* gs);
void calculate_food_map(GameState* gs);

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

#endif
