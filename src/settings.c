#include "settings.h"

#include <assert.h>

#include <ink_data.h>
#include <ink_query.h>

#include "idents.h"
#include "prefs.h"
#include "reader.h"
#include "stretchy_buffer.h"
#include "writer.h"
#include <config.h>


#define NUM_BACKROUND_PLAYERS 6
#define NUM_START_CONTROLS 6
#define NUM_START_NAMES 5
#define NUM_COLORS 9



static const char* start_names[NUM_START_NAMES] = {
    "Antler",
    "Buck",
    "Chuck",
    "Dusty",
    "Earl",
};

static const int colors[NUM_COLORS] = {
    0x2affc3ff,
    0xff893bff,
    0x7ff9ffff,
    0x93a4ffff,
    0xffff74ff,
    0xff639fff,
    0x97ff5eff,
    0xffc3f1ff,
    0xcba5ffff,
};

static const SDL_Keycode controls[NUM_START_CONTROLS][4] = {
    {SDLK_UP, SDLK_RIGHT, SDLK_DOWN, SDLK_LEFT},
    {SDLK_w, SDLK_d, SDLK_s, SDLK_a},
    {SDLK_t, SDLK_h, SDLK_g, SDLK_f},
    {SDLK_i, SDLK_l, SDLK_k, SDLK_j},
    {SDLK_MINUS, SDLK_RIGHTBRACKET, SDLK_LEFTBRACKET, SDLK_p},
    {SDLK_KP_8, SDLK_KP_6, SDLK_KP_5, SDLK_KP_4},
};

extern struct ink* ink;
static inkd_listener* listener = 0;
//static inkd* worm_settings = 0;
static int num_players;

static int get_worm_game_id(int id) {
    assert(id < num_players);
    int count = 0;
    ink_query q;
    ink_query_init(&q);
    inkd source = ink_source(ink, I_settings);
    for (int i=0; i<id; i++) {
        ink_query_int(&q, I_id, i);
        inkd ws = ink_query_get(&q, source, I_worm_settings);
        if (inkd_get_int(ws, I_control_type) != ControlTypeNone) count ++;
    }
    ink_query_deinit(&q);
    return count;
}

void set_gamestate_settings(GameState* gs) {
    if (gs->type == GAME_TYPE_NORMAL || gs->type == GAME_TYPE_MAP_PREVIEW) {
        inkd source = ink_source(ink, I_settings);
        ink_query q;
        ink_query_init(&q);
        for (int i=0; i<num_players; i++) {
            ink_query_int(&q, I_id, i);
            inkd d = ink_query_get(&q, source, I_worm_settings);
            if (inkd_get_int(d, I_control_type) != ControlTypeNone) {
                Worm* w = sbadd(gs->worms, 1);
                memset(w, 0, sizeof(Worm));
                w->color = inkd_get_color(d, I_color);
                if (gs->type == GAME_TYPE_NORMAL && inkd_get_int(d, I_control_type) == ControlTypePlayer) {
                    w->controls[0] = inkd_get_int(d, I_north_key);
                    w->controls[1] = inkd_get_int(d, I_east_key);
                    w->controls[2] = inkd_get_int(d, I_south_key);
                    w->controls[3] = inkd_get_int(d, I_west_key);
                } else {
                    memset(w->controls, 0, sizeof(SDL_Keycode) * 4);
                }
                w->lives = 5;
                w->points = 0;

                if (gs->type == GAME_TYPE_NORMAL) {
                    inkd game_source = ink_source(ink, I_game);
                    w->data = inkd_append(game_source, I_worms);
                    inkd_charp(w->data, I_name, inkd_get_charp(d, I_name));
                    inkd_color(w->data, I_color, w->color);
                    inkd_int(w->data, I_time_since_last_ate, 1000000);
                } else {
                    w->data = NULL;
                }
            }
        }
        ink_query_deinit(&q);

        gs->framerate = inkd_get_int(ink_source(ink, I_settings), I_start_speed);
    } else {
        sbresize(gs->worms, NUM_BACKROUND_PLAYERS);
        memset(gs->worms, 0, sizeof(Worm) * NUM_BACKROUND_PLAYERS);
        sbforeachp(Worm* w, gs->worms) {
            w->color = colors[stb__counter % NUM_COLORS];
        }
        gs->framerate = 150;
    }
}

static inkd append_worm_settings() {
    inkd source = ink_source(ink, I_settings);
    int i = num_players ++;
    printf("Settings for %d worms.\n", num_players);
    inkd ws = inkd_append(source, I_worm_settings);

    inkd_int(ws, I_id, i);
    inkd_color(ws, I_color, colors[i % NUM_COLORS]);
    inkd_int(ws, I_control_type, i < 6 ? ControlTypePlayer : ControlTypeComputer);
    inkd_charp(ws, I_name, start_names[i % NUM_START_NAMES]);
    inkd_int(ws, I_north_key, controls[i % NUM_START_CONTROLS][0]);
    inkd_int(ws, I_east_key, controls[i % NUM_START_CONTROLS][1]);
    inkd_int(ws, I_south_key, controls[i % NUM_START_CONTROLS][2]);
    inkd_int(ws, I_west_key, controls[i % NUM_START_CONTROLS][3]);
    inkd_charp(ws, I_north_string, SDL_GetKeyName(controls[i % NUM_START_CONTROLS][0]));
    inkd_charp(ws, I_south_string, SDL_GetKeyName(controls[i % NUM_START_CONTROLS][1]));
    inkd_charp(ws, I_east_string, SDL_GetKeyName(controls[i % NUM_START_CONTROLS][2]));
    inkd_charp(ws, I_west_string, SDL_GetKeyName(controls[i % NUM_START_CONTROLS][3]));
    return ws;
}

static void remove_worm_settings(int id) {
    ink_query q;
    ink_query_init(&q);
    ink_query_int(&q, I_id, id);
    inkd ws = ink_query_maybe_get(&q, ink_source(ink, I_settings), I_worm_settings);
    if (ws) {
        for (int i=id+1; i<num_players; i++) {
            ink_query_int(&q, I_id, i);
            inkd_int(ink_query_maybe_get(&q, ink_source(ink, I_settings), I_worm_settings), I_id, i-1);
        }
        inkd_remove(ws);
        num_players --;
    }
    ink_query_deinit(&q);
}

static void destroy_worm_settings() {
    inkd_clear(ink_source(ink, I_settings), I_worm_settings);
    num_players = 0;
}

void save_settings() {
    uint8_t* data = malloc(4);
    memset(data, 0, 4);

    inkd source = ink_source(ink, I_settings);

    w_i32(&data, inkd_get_int(source, I_start_speed));
    w_i32(&data, num_players);

    ink_query q;
    ink_query_init(&q);

    for (int id=0; id<num_players; id ++) {
        ink_query_int(&q, I_id, id);
        inkd ws = ink_query_get(&q, source, I_worm_settings);
        w_i32(&data, inkd_get_color(ws, I_color));
        w_i32(&data, inkd_get_int(ws, I_control_type));
        w_str(&data, inkd_get_charp(ws, I_name));
        w_i32(&data, inkd_get_int(ws, I_north_key));
        w_i32(&data, inkd_get_int(ws, I_east_key));
        w_i32(&data, inkd_get_int(ws, I_south_key));
        w_i32(&data, inkd_get_int(ws, I_west_key));
    }
    ink_query_deinit(&q);

    FILE* fp = 0;
 
#ifdef __EMSCRIPTEN__
    fp = fopen("data/worm.settings", "wb");
#else
    {
        char path[1024];
        if (prefs_dir_file(path, 1024, prefs_config_dir(), "worms.settings") != 0) {
            fprintf(stderr, "Failed to save settings.\n");
            goto cleanup;
        }
        fp = fopen(path, "wb");
    }
#endif
    if (!fp) {
        fprintf(stderr, "Failed to save settings.\n");
        goto cleanup;
    }
    uint32_t data_length;
    memcpy(&data_length, data, 4);
    assert(data_length >= 4);
    fwrite((data+4), data_length, 1, fp);
    fclose(fp);
cleanup:
    free(data);
}

static void load_settings() {
    FILE* fp = prefs_get_config_fp("worms.settings");
    if (!fp) return;
    fseek(fp, 0, SEEK_END);
    int count = ftell(fp);
    rewind(fp);
    uint8_t* buffer = malloc(count);
    fread(buffer, count, 1, fp);
    fclose(fp);

    uint8_t* r = buffer;

    inkd source = ink_source(ink, I_settings);
    inkd_clear_all(source);

    inkd_int(source, I_start_speed, r_i32(&r));
    int np = r_i32(&r);
    for (int id=0; id<np; id ++) {
        inkd ws = append_worm_settings();
        inkd_color(ws, I_color, r_i32(&r));
        inkd_int(ws, I_control_type, r_i32(&r));
        inkd_charp(ws, I_name, r_str(&r));

        inkd_int(ws, I_north_key, r_i32(&r));
        inkd_int(ws, I_east_key, r_i32(&r));
        inkd_int(ws, I_south_key, r_i32(&r));
        inkd_int(ws, I_west_key, r_i32(&r));
        inkd_charp(ws, I_north_string, SDL_GetKeyName(inkd_get_int(ws, I_north_key)));
        inkd_charp(ws, I_east_string, SDL_GetKeyName(inkd_get_int(ws, I_east_key)));
        inkd_charp(ws, I_south_string, SDL_GetKeyName(inkd_get_int(ws, I_south_key)));
        inkd_charp(ws, I_west_string, SDL_GetKeyName(inkd_get_int(ws, I_west_key)));
    }
    assert(np == num_players);

    free(buffer);
}

void settings_init() {
    if (listener) settings_shutdown();
    listener = inkd_listener_new(0);
    inkd source = ink_source(ink, I_settings);
    inkd_clear_all(source);
    inkd_add_listener(source, listener);
    load_settings();
    inkd_rebuild_begin(source, I_colors);
    for (int i=0; i<NUM_COLORS; i++) {
        inkd d = inkd_append(source, I_colors);
        inkd_color(d, I_color, colors[i]);
    }
    inkd_rebuild_end(source, I_colors);
}

void settings_shutdown() {
    if (!listener) return;
    save_settings();
    destroy_worm_settings();
    inkd_listener_delete(listener);
    listener = 0;
}


void settings_step() {
    if (!listener) return;

    bool new_game = false;
    GameState* gs = get_gamestate();

    struct ink_data_event ev;
    while (inkd_listener_poll(listener, &ev)) {
        switch (ev.type) {
        case INK_DATA_EVENT_IMPULSE:
            switch (ev.field) {
                case I_save_settings:
                    save_settings();
                    break;
                case I_remove_worm:
                    remove_worm_settings(inkd_get_int(ev.data, I_id));
                    break;
                case I_append_worm:
                    append_worm_settings();
                    break;
                default:
                    break;
            }
            break;
        case INK_DATA_EVENT_CHANGE: 
            switch (ev.field) {
                case I_color:
                    if (gs) gs->worms[get_worm_game_id(inkd_get_int(ev.data, I_id))].color = inkd_get_color(ev.data, I_color);
                    break;
                case I_start_speed:
                    if (gs) {
                        gs->base_framerate = inkd_get_int(ev.data, ev.field);
                        gs->framerate = gs->base_framerate;
                        gs->steps_till_base_framerate = 0;
                    }
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
        }
    }
    if (new_game) {
        char path[2048];
        strcpy(path, inkd_get_charp(ink_source(ink, I_lobby), I_path));
        if (strlen(path) > 0) {
            start_game(path, GAME_TYPE_MAP_PREVIEW);
        }
    }
}


bool settings_event(SDL_Event* ev) {
    if (!listener) return false;
    if (ev->type == SDL_KEYDOWN) {
        ink_query q;
        ink_query_init(&q);
        for (int id=0; id<num_players; id++) {
            ink_query_int(&q, I_id, id);
            inkd ws = ink_query_get(&q, ink_source(ink, I_settings), I_worm_settings);
            if (inkd_get_bool(ws, I_north_fill)) {
                inkd_int(ws, I_north_key, ev->key.keysym.sym);
                inkd_charp(ws, I_north_string, SDL_GetKeyName(ev->key.keysym.sym));
                inkd_bool(ws, I_north_fill, false);
                return true;
            } else if (inkd_get_bool(ws, I_east_fill)) {
                inkd_int(ws, I_east_key, ev->key.keysym.sym);
                inkd_charp(ws, I_east_string, SDL_GetKeyName(ev->key.keysym.sym));
                inkd_bool(ws, I_east_fill, false);
                return true;
            } else if (inkd_get_bool(ws, I_south_fill)) {
                inkd_int(ws, I_south_key, ev->key.keysym.sym);
                inkd_charp(ws, I_south_string, SDL_GetKeyName(ev->key.keysym.sym));
                inkd_bool(ws, I_south_fill, false);
                return true;

            } else if (inkd_get_bool(ws, I_west_fill)) {
                inkd_int(ws, I_west_key, ev->key.keysym.sym);
                inkd_charp(ws, I_west_string, SDL_GetKeyName(ev->key.keysym.sym));
                inkd_bool(ws, I_west_fill, false);
                return true;
            }
        }
        ink_query_deinit(&q);
    }
    return false;
}
