#include "food.h"

#include <assert.h>
#include <stdlib.h>

#include "map.h"
#include "queue.h"
#include "stretchy_buffer.h"
#include "worm.h"

#define LEN_FOOD_WEIGHTS 20
#define MAX_DISTANCE 60000

static const enum food_type food_weights[LEN_FOOD_WEIGHTS] = {
    FOOD_TYPE_APPLE,
    FOOD_TYPE_APPLE,
    FOOD_TYPE_APPLE,
    FOOD_TYPE_APPLE,
    FOOD_TYPE_APPLE,
    FOOD_TYPE_BANANA,
    FOOD_TYPE_BANANA,
    FOOD_TYPE_BANANA,
    FOOD_TYPE_BANANA,
    FOOD_TYPE_BANANA,
    FOOD_TYPE_CHERRY,
    FOOD_TYPE_CHERRY,
    FOOD_TYPE_CHERRY,
    FOOD_TYPE_DOUGHNUT,
    FOOD_TYPE_DOUGHNUT,
    FOOD_TYPE_DOUGHNUT,
    FOOD_TYPE_DIMOND,
    FOOD_TYPE_HEART,
    FOOD_TYPE_CLOCK_FAST,
    FOOD_TYPE_CLOCK_FAST,
};

static bool area_is_clear_for_food(GameState* gs, int x, int y) {
    // Ensure no wrapping
    if (x > gs->map->width-2 || y > gs->map->height-2) return false;
    for (int fy=y; fy<y+2; fy++) {
        for (int fx=x; fx<x+2;fx++) {
            if (gs->food_map[fy*gs->map->width+fx] > MAX_DISTANCE) return false;
            if (gs->food_map[fy*gs->map->width+fx] == 0) return false;
        }
    }
    return true;
}

static Food* more_food(GameState* gs) {
    sbforeachp(Food* f, gs->food) {
        if (f->type == FOOD_TYPE_END) {
            return f;
        }
    }
    Food* f = sbadd(gs->food, 1);
    return f;
}

void add_food(GameState* gs) {
    for (int i=0; i<10000; i++) {
        int x = rand() % (gs->map->width-1);
        int y = rand() % (gs->map->height-1);
        if (area_is_clear_for_food(gs, x, y)) {
            Food* f = more_food(gs);
            f->x = x;
            f->y = y;
            f->type = food_weights[rand() % LEN_FOOD_WEIGHTS];
            f->eating = 0;
            f->time = gs->map->width * gs->map->height / 4;
            return;
        }
    }
}

void calculate_food_map(GameState* gs) {
    pathint* fm = gs->food_map;
    // Initalize it with walls as obstacles
    for (pathint i=0; i<gs->map->width*gs->map->height; i++) {
        fm[i] = (gs->map->tiles[i] == MAP_TILE_TYPE_WALL) ? MAX_DISTANCE+1 : MAX_DISTANCE;
    }
    // Add worms as obstacles
    sbforeachp(const Worm* w, gs->worms) {
        for (pathint i=0; i<queue_count(w->path); i++) {
            fm[queue_getv(w->path, i)] = MAX_DISTANCE+1;
        }
    }
    static pathint* queue = 0; // queue
    // Add food as targets
    sbforeachp(const Food* f, gs->food) {
        for (int fy=0; fy<2; fy++) {
            for (int fx=0; fx<2; fx++) {
                pathint p = flatten(gs->map, f->x + fx, f->y + fy);
                queue_push(queue, p);
                gs->food_map[p] = 0;
            }
        }
    }
    // Do breadth-first search
    while (queue_count(queue)) {
        pathint c = queue_pop(queue);
        for (enum direction i=0; i<DIRECTION_END; i++) {
           pathint n = apply_direction(gs->map, i, c);
           if (fm[c] + 1 < fm[n] && fm[n] <= MAX_DISTANCE) {
               fm[n] = fm[c] + 1;
               queue_push(queue, n);
           }
        }
    }
    queue_clear(queue);
}
