#ifndef __GLADUPE_PREFS__
#define __GLADUPE_PREFS__

#include <stdio.h>

void adds(char** strp, const char* src);
void addns(char** strp, const char* src, size_t len);
const char* prefs_data_dir(void);
const char* prefs_config_dir(void);
int prefs_dir_file(char* out, size_t max_out, const char* dir, const char* filename);
FILE* prefs_get_config_fp(const char* filename);
int prefs_get_path_to_data_file(char* out, size_t max_out, const char* filename);

#endif
