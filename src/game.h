#ifndef __GLADUPE_GAME__
#define __GLADUPE_GAME__


#include <SDL2/SDL.h>
#include <ink_data.h>

#include "map.h"

enum game_type {
    GAME_TYPE_NORMAL=0,
    GAME_TYPE_MAIN_MENU,
    GAME_TYPE_MAP_PREVIEW,
    GAME_TYPE_MAP_EDITOR
};

enum food_type {
    FOOD_TYPE_APPLE=0,
    FOOD_TYPE_BANANA,
    FOOD_TYPE_CHERRY,
    FOOD_TYPE_DOUGHNUT,
    FOOD_TYPE_DIMOND,
    FOOD_TYPE_HEART,
    FOOD_TYPE_CLOCK_FAST,
    FOOD_TYPE_CLOCK_SLOW,
    FOOD_TYPE_END
};


typedef struct food {
    int time;
    pathint x;
    pathint y;
    enum food_type type;
    unsigned char eating;
} Food;


enum direction {
    DIRECTION_NORTH=0,
    DIRECTION_EAST,
    DIRECTION_SOUTH,
    DIRECTION_WEST,
    DIRECTION_END
};

enum worm_event {
    WORM_EVENT_NONE=0,
    WORM_EVENT_EAT,
    WORM_EVENT_KILL_WALL,
    WORM_EVENT_KILL_WORM,
    WORM_EVENT_HOLE,
    WORM_EVENT_END
};

typedef struct worm {
    SDL_Keycode controls[4];
    pathint* path; // Queue
    enum worm_event* events; // Queue
    enum direction* direction_queue; // Queue
    inkd data;
    uint32_t color;
    int points;
    pathint tail; // Where the last segment on a worm was last step
    uint16_t time_since_last_ate;
    unsigned short int growth;
    short int lives;
    enum direction direction;
    bool reverse;
} Worm;

typedef struct gamestate {
    Map* map;
    Worm* worms; // stretchy_buffer
    Food* food; // stretchy_buffer
    pathint* food_map;
    int* noise;
    unsigned int steps_left_in_game;
    unsigned short int framerate;
    unsigned short int base_framerate;
    unsigned char steps_till_base_framerate;
    enum game_type type;
} GameState;


GameState* get_gamestate();
GameState* start_game(const char* path, enum game_type type);
void end_game();
void game_step(int dt);
#ifdef USE_INK_RUNNER
bool game_event(struct ink_event* ev);
#else
bool game_event(SDL_Event* ev);
#endif

#endif
