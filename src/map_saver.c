#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "map_saver.h"
#include "prefs.h"
#include "writer.h"

int get_path(char* path, int max_out, const char* name) {
    // Make ascii name and save to file
    char map_name[128] = {0};
    for (unsigned i=0; i<strlen(name) && i<127-8; i++) {
        char c = name[i];
        if ((c < 'A' || c > 'z') && !(c >= '0' && c <= '9')) {
            c = '_';
        }
        map_name[i] = c;
    }
    strcat(map_name, ".gladupe");
    return prefs_dir_file(path,
            max_out,
#ifdef __EMSCRIPTEN__
            "data/",
#else
            prefs_data_dir(),
#endif
            map_name);
}

void save_map(Map* m) {
    uint8_t* data = 0;
    data = malloc(4);
    memset(data, 0, 4);
    w_str(&data, m->name);
    
    w_i32(&data, m->width);
    w_i32(&data, m->height);


    assert(sizeof(*m->tiles) == sizeof(int32_t));
    for (int i=0; i<m->width*m->height; i++) {
        w_i32(&data, m->tiles[i]);
    }

    char path[2048];
    if (get_path(path, 2048, m->name) != 0) {
        fprintf(stderr, "Failed to save map.\n");
    } else {
        printf("Saving map to %s\n", path);
        FILE* fp = fopen(path, "wb");
        uint32_t data_length;
        memcpy(&data_length, data, 4);
        fwrite(data+4,  data_length, 1, fp);
        fclose(fp);
    }
    free(data);
}
