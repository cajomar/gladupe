#include <ink.h>
#include <ink_data.h>
#include <ink_experimental.h>
#include <stdio.h>
#include <string.h>

#include "lobby.h"
#include "idents.h"
#include "map_loader.h"
#include "game.h"
#include "settings.h"

extern struct ink* ink;
static inkd_listener* listener = 0;


void lobby_init() {
    if (listener) return;
    listener = inkd_listener_new(0);
    inkd source = ink_source(ink, I_lobby);
    inkd_clear_all(source);
    inkd_add_listener(source, listener);
    load_map_list();
    settings_init();
}

void lobby_shutdown() {
    if (!listener) return;
    inkd_listener_delete(listener);
    listener = 0;
    settings_shutdown();
}

static void sg(enum game_type gt) {
    start_game(inkd_get_charp(ink_source(ink, I_map_selection), I_path), gt);
}

void lobby_step() {
    if (!listener) return;

    struct ink_data_event ev;
    while (inkd_listener_poll(listener, &ev)) {
        switch (ev.type) {
        case INK_DATA_EVENT_IMPULSE:
            switch (ev.field) {
            case I_play_again:
                end_game();
                sg(GAME_TYPE_NORMAL);
                break;
            case I_selected_map:
                sg(GAME_TYPE_MAP_PREVIEW);
                break;
            default:
                break;
            }
            break;
        case INK_DATA_EVENT_CHANGE:
            switch (ev.field) {
            case I_in_game:
                if (inkd_get_bool(ev.data, ev.field)) {
                    sg(GAME_TYPE_NORMAL);
                } else { 
                    end_game();
                }
                break;
            default:
                break;
            }
        default:
            break;
        }
    }
}
