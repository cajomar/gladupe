#include "game.h"

#include <assert.h>
#include <time.h>

#include <ink_data.h>
#include <ink_query.h>

#include "food.h"
#include "idents.h"
#include "map_loader.h"
#include "queue.h"
#include "render.h"
#include "settings.h"
#include "stretchy_buffer.h"
#include "worm.h"

#define DEATH_PENALTY 10


extern struct ink* ink;

static int time_left_till_frame = 0;
static int steps_left_in_game;

static GameState* gs = 0;

// should be used sparingly
GameState* get_gamestate() {
    return gs;
}

static int check_for_worm_events(npint id) {
    Worm* w = &gs->worms[id];
    if (queue_last(w->events) == WORM_EVENT_KILL_WORM) {
        return id;
    }
    int hx = queue_last(w->path) % gs->map->width;
    int hy = queue_last(w->path) / gs->map->width;
    // Check for a hole
    // Check if the second section is over a hole, if the first is not a hole,
    // and if the worm hasn't just come from a hole. If we didn't do the last
    // condition then a worm would be forever going through holes.
    if (queue_count(w->path) > 1 &&
            gs->map->tiles[queue_getv(w->path, -1)] != MAP_TILE_TYPE_HOLE &&
            gs->map->tiles[queue_getv(w->path, -2)] == MAP_TILE_TYPE_HOLE &&
            gs->map->tiles[queue_count(w->path) == 2 ? w->tail : queue_getv(w->path, -3)] != MAP_TILE_TYPE_HOLE) {

        static pathint* holes = 0; // stretchy_buffer
        sbresize(holes, 0);

        // Find all the holes, shuffling them as we go
        for (int l=0; l<gs->map->width*gs->map->height; l++) {
            if (gs->map->tiles[l] != MAP_TILE_TYPE_HOLE) continue;
            int j = rand() % (sbcount(holes) + 1);
            if (j == sbcount(holes)) {
                sbpush(holes, l);
            } else {
                sbpush(holes, holes[j]);
                holes[j] = l;
            }
        }
        if (sbcount(holes) > 1) {
            sbforeachv(pathint hole, holes) {
                if (hole == queue_getv(w->path, -2))
                    continue;
                // needs to be empty
                if (any_worm_at(gs, hole))
                    continue;
                // find the loc where the worm will be next step
                int side = apply_direction(gs->map, queue_empty(w->direction_queue) ? w->direction : queue_peek(w->direction_queue), hole);
                // make sure it's clear
                if (gs->map->tiles[side] != MAP_TILE_TYPE_EMPTY)
                    continue;
                if (any_worm_at(gs, side))
                    continue;
                // if it gets this far, it's a good hole
                queue_getv(w->path, -1) = hole;
                queue_getv(w->events, -1) = WORM_EVENT_HOLE;
                return 0;
            }
        }
    }
    // check for a worm
    sbforeachp(Worm* o, gs->worms) {
        if (o != w && queue_last(o->path) == queue_last(w->path)) {
            queue_getv(w->events, -1) = WORM_EVENT_KILL_WORM;
            queue_getv(o->events, -1) = WORM_EVENT_KILL_WORM;
            return id;
        }
        for (pathint seg=0; seg<queue_count(o->path)-1; seg++) {
            if (queue_getv(o->path, seg) == queue_last(w->path)) {
                queue_getv(w->events, -1) = WORM_EVENT_KILL_WORM;
                return stb__counter;
            }
        }
    }
    // check for a wall
    if (gs->map->tiles[queue_last(w->path)] == MAP_TILE_TYPE_WALL) {
        queue_getv(w->events, -1) = WORM_EVENT_KILL_WALL;
        return 0;
    }
    // check for food
    sbforeachp(Food* f, gs->food) {
        if ((hx - f->x == 0 || hx - f->x == 1) && (hy-f->y == 0 || hy-f->y == 1)) {
            f->eating ++;
            queue_getv(w->events, -1) = WORM_EVENT_EAT;
            return stb__counter;
        }
    }
    return 0;
}

GameState* start_game(const char* path, enum game_type type) {
    if (gs) end_game();
    srand(time(0));
    inkd game_source = ink_source(ink, I_game);
    inkd_clear_all(game_source);

    assert(!gs);
    gs = calloc(1, sizeof(GameState));

    gs->map = load_map(path);
    if (!gs->map) {
        gs->map = get_default_map();
    }

    gs->noise = malloc(gs->map->width * gs->map->height * sizeof(*gs->noise));
    for (pathint i=0; i<gs->map->width*gs->map->height; i++) gs->noise[i] = rand();

    gs->type = type;

    set_gamestate_settings(gs);
    for (int i=0; i<sbcount(gs->worms); i++) {
        worm_restart(gs, i);
    }
    gs->base_framerate = gs->framerate;
    gs->steps_left_in_game = 1;
    gs->steps_till_base_framerate = 1;

    gs->food_map = malloc(gs->map->width * gs->map->height * sizeof(*gs->food_map));
    calculate_food_map(gs);


    time_left_till_frame = type == GAME_TYPE_NORMAL ? 3000 : 0;
    inkd_int(game_source, I_countdown, time_left_till_frame);

    for (int i=0; i<sbcount(gs->worms); i++) {
        add_food(gs);
    }
    calculate_food_map(gs);
    steps_left_in_game = 10000;
    return gs;
}

void end_game() {
    assert(gs);
    sbforeachp(Worm* w, gs->worms) {
        queue_free(w->path);
        queue_free(w->events);
        queue_free(w->direction_queue);
    }
    sbfree(gs->worms);
    destroy_map(gs->map);
    sbfree(gs->food);
    free(gs->food_map);
    free(gs->noise);
    free(gs);
    gs = 0;
}

static bool check_if_near_worm_head(pathint pos, npint checking_id) {
    int x = pos % gs->map->width;
    int y = pos / gs->map->width;
    sbforeachp(Worm* w, gs->worms) {
        if (stb__counter == checking_id) continue;
        if (abs(queue_last(w->path)%gs->map->width - x) <= 1 &&
                abs(queue_last(w->path)/gs->map->width - y) <= 1) {
            return true;
        }
    }
    return false;
}

void game_step(int dt) {
    if (!gs) return;

    inkd source = ink_source(ink, I_game);

    if (inkd_get_bool(source, I_paused) || inkd_get_bool(source, I_wrapping_up)) {
        render(gs, 0, 0);
        time_left_till_frame = 3000;
        inkd_int(source, I_countdown, time_left_till_frame);
        return;
    }
    time_left_till_frame -= dt;
    inkd_int(source, I_countdown, inkd_get_int(source, I_countdown)-dt);

    sbforeachp(Worm* w, gs->worms) {
        w->time_since_last_ate += dt;
        if (w->data) inkd_int(w->data, I_time_since_last_ate, w->time_since_last_ate);
    }

    if (time_left_till_frame < 0.0001f) {
        gs->steps_till_base_framerate = MAX(gs->steps_till_base_framerate-1, 1);

        assert(gs->base_framerate >= gs->framerate);
        gs->framerate += (gs->base_framerate - gs->framerate) / gs->steps_till_base_framerate;

        time_left_till_frame = gs->framerate;
        if (gs->type == GAME_TYPE_NORMAL) inkd_int(source, I_speed, gs->framerate);

        if (gs->type == GAME_TYPE_NORMAL && steps_left_in_game -- < 0) {
            inkd_trigger(source, I_show_game_wrapup);
        }

        sbforeachp(Worm* w, gs->worms) {
            w->time_since_last_ate += dt;

            if (w->controls[0]) {
                if (queue_count(w->direction_queue)) {
                    w->direction = queue_pop(w->direction_queue);
                }
            } else {
                enum direction d = w->direction;
                for (enum direction i=0; i<DIRECTION_END; i++) {
                    if (i == opposite_direction(d)) continue;
                    pathint p1 = apply_direction(gs->map, d, queue_last(w->path));
                    pathint p2 = apply_direction(gs->map, i, queue_last(w->path));
                    if (gs->food_map[p2] < gs->food_map[p1]) d = i;
                }
                w->direction = d;
            }
        }
        sbforeachp(Worm* w, gs->worms) {
            worm_move(w, gs->map);
        }

        int event_data[sbcount(gs->worms)];
        for (int i=0; i<sbcount(gs->worms); i++) {
            event_data[i] = check_for_worm_events(i);
        }

        sbforeachp(Worm* w, gs->worms) {
            int ed = event_data[stb__counter];
            switch (queue_last(w->events)) {
                case WORM_EVENT_NONE:
                    break;
                case WORM_EVENT_EAT:
                    {
                        Food* f = &gs->food[ed];
                        assert(f->eating);
                        switch (f->type) {
                            case FOOD_TYPE_APPLE:
                                worm_multiply_growth(w, 1.5f/(float)f->eating);
                                worm_add_points(w, 5/f->eating);
                                break;
                            case FOOD_TYPE_BANANA:
                                worm_add_growth(w, 20/f->eating);
                                worm_add_points(w, 7/f->eating);
                                break;
                            case FOOD_TYPE_CHERRY:
                                worm_multiply_growth(w, 0.5f/(float)f->eating);
                                worm_add_points(w, 10/f->eating);
                                break;
                            case FOOD_TYPE_DOUGHNUT:
                                worm_multiply_growth(w, 2.0f/(float)f->eating);
                                worm_add_points(w, 1/f->eating);
                                break;
                            case FOOD_TYPE_HEART:
                                w->lives ++;
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                case WORM_EVENT_KILL_WALL:
                    worm_add_points(w, -DEATH_PENALTY);
                    if (gs->type == GAME_TYPE_NORMAL) w->lives --;
                    if (w->lives <= 0 && gs->type == GAME_TYPE_NORMAL) inkd_trigger(source, I_show_game_wrapup);
                    else worm_restart(gs, stb__counter);
                    break;
                case WORM_EVENT_KILL_WORM:
                    if (event_data[stb__counter] != stb__counter) {
                        worm_add_points(&gs->worms[event_data[stb__counter]], DEATH_PENALTY);
                    }
                    worm_add_points(w, -DEATH_PENALTY);
                    if (gs->type == GAME_TYPE_NORMAL) w->lives --;
                    if (w->lives <= 0 && gs->type == GAME_TYPE_NORMAL) inkd_trigger(source, I_show_game_wrapup);
                    else worm_restart(gs, stb__counter);
                    break;
                case WORM_EVENT_HOLE:
                    break;
                default:
                    printf("WARNING: Unexpected worm event type %d\n", queue_last(w->events));
                    break;
            }
        }
        sbforeachp(Food* f, gs->food) {
            if (f->eating) {
                if (f->type == FOOD_TYPE_CLOCK_FAST || f->type == FOOD_TYPE_CLOCK_SLOW) {
                    gs->framerate = MAX(gs->base_framerate * 0.5, 1);
                    time_left_till_frame = gs->framerate;
                    gs->steps_till_base_framerate += 50;
                } else if (f->type == FOOD_TYPE_DIMOND) {
                    for (int i=0; i<sbcount(gs->worms); ++i) {
                        Worm* w = &gs->worms[i];
                        // TODO make sure that len(event_data) == sbcount(gs->worms)
                        if (queue_last(w->events) != WORM_EVENT_EAT || event_data[i] != stb__counter) {
                            w->reverse = true;
                        }
                    }
                }
                f->type = FOOD_TYPE_END;
                add_food(gs);
            } else if (f->time-- <= 0) {
                f->type = FOOD_TYPE_END;
                add_food(gs);
            }
        }
        calculate_food_map(gs);

        sbforeachp(Worm* w, gs->worms) {
            if (w->data) {
                inkd_int(w->data, I_lives, w->lives);
                inkd_int(w->data, I_points, w->points);
                inkd_int(w->data, I_time_since_last_ate, w->time_since_last_ate);
            }
        }
    }
    render(gs, dt, time_left_till_frame);
}

bool game_event(SDL_Event* ev) {
    if (!gs) return false;
    if (ev->type == SDL_KEYDOWN) {
        sbforeachp(Worm* w, gs->worms) {
            if (!w->controls[0]) continue;
            enum direction queued = queue_empty(w->direction_queue) ? w->direction : queue_last(w->direction_queue);
            enum direction opposite = opposite_direction(queued);
            for (enum direction d=0; d<DIRECTION_END; d++) {
                if (ev->key.keysym.sym == w->controls[d] && d != queued && d != opposite) {
                    queue_push(w->direction_queue, d);
                    return true;
                }
            }
        }
    }
    return false;
}
