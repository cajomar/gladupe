#include "prefs.h"

#include <config.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#define MAX_PATH 128

const char* prefs_global_data_dir(void) {
    static char* dir = 0;
    if (dir == 0) {
        const char* v = getenv("SNAP");
        const char* d = "/share/gladupe/";
        if (v == NULL) {
            fprintf(stderr, "Couldn't get value of SNAP environment variable\n");
            return NULL;
        }
        dir = malloc(strlen(v) + strlen(d) + 1);
        if (dir == NULL) {
            fprintf(stderr, "malloc() failed\n");
            return NULL;
        }
        strcpy(dir, v);
        strcat(dir, d);
    }
    return dir;
}

const char* prefs_data_dir(void) {
    static char* dir = 0;
    if (dir == 0) {
        const char* v = getenv("SNAP_USER_COMMON");
        if (v == NULL) {
            fprintf(stderr, "Couldn't get value of SNAP_USER_COMMON environment variable\n");
            return NULL;
        }
        dir = malloc(strlen(v) + 1 + 1);
        if (dir == NULL) {
            fprintf(stderr, "malloc() failed\n");
            return NULL;
        }
        strcpy(dir, v);
        strcat(dir, "/");
    }
    return dir;
}


const char* prefs_config_dir(void) {
    return prefs_data_dir();
}

int prefs_dir_file(char* out, size_t max_out, const char* dir, const char* filename) {
    memset(out, 0, max_out);
    if (strlen(dir) + strlen(filename) + 1 > max_out) return -1;
    strcpy(out, dir);
    strcat(out, filename);
    return 0;
}

#define CHECK_DIR(PATH) \
    prefs_dir_file(p, MAX_PATH, (PATH), filename); \
    fp = fopen(p, "rb"); \
    if (fp) return fp;

FILE* prefs_get_data_fp(const char* filename) {
    char p[MAX_PATH];
    FILE* fp;

    CHECK_DIR(prefs_data_dir())
    CHECK_DIR("data/")
    CHECK_DIR(prefs_global_data_dir())

    fprintf(stderr, "no file named %s in %s, %s, or %s\n",
            filename, 
            prefs_data_dir(),
            "data/",
            (char*) prefs_global_data_dir());
    return NULL;
} 

FILE* prefs_get_config_fp(const char* filename) {
    char p[MAX_PATH];
    FILE* fp;

    CHECK_DIR(prefs_config_dir())
    CHECK_DIR("data/")
    CHECK_DIR(prefs_global_data_dir())

    fprintf(stderr, "Couldn't find %s in %s, %s, or %s\n",
            filename, 
            prefs_data_dir(),
            "data/",
            prefs_global_data_dir());
    return NULL;
} 
#undef CHECK_DIR

int prefs_get_data_file_path(char* out, size_t max_out, const char* filename) {
#define CHECK_DIR(PATH) \
    if (prefs_dir_file(out, max_out, PATH, filename) == 0 && access(out, R_OK) == 0) { \
        printf("Found %s at %s\n", filename, out); \
        return 0; \
    }
    CHECK_DIR(prefs_data_dir())
    CHECK_DIR("data/")
    CHECK_DIR(prefs_global_data_dir())
#undef DATA_CHECK_DIR
    fprintf(stderr, "Couldn't find %s in %s, %s, or %s\n", 
            filename,
            prefs_data_dir(),
            "data/",
            prefs_global_data_dir());
    return -1;
}
