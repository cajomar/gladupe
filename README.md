# Gladupe #
A multi-player snake game.  

If you're wanting to play it on linux, check out the [downloads page](https://bitbucket.org/cajomar/gladupe/downloads) or get it from the [snap store](https://snapcraft.io/gladupe).  
If you've only got Windows then you could try running it on the [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10). I haven't tried it though.

## Dependencies ##
[SDL2](https://libsdl.org), [OpenGL](https://www.opengl.org), [lib2d](https://lib2d.com), and [ink](https://uiink.com).  
The [compiled binaries](https://bitbucket.org/cajomar/gladupe/downloads) come with lib2d and ink, so you'll only need SDL2 and OpenGL.  

## Building ##
``` Console
# Move to gladupe's root directory:
$ cd gladupe
# Generate the build files:
$ cmake -B build
# Build gladupe:
$ cmake --build build
# Run the program:
$ ./build/gladupe
```

## TODO ##
* Make dimonds reverse everyone else
* Do something to make worm jumping (holes, dieing, etc) more obvious
* Make the speed picker a sliding bar?
* Make hole graphics a bit nicer?
* Change the holes from being a tile type into an external object (like food)?
* Update the preview game every time you add/subtract a worm by reallocating gamestate?
* Add networking support
* An android build?
* Improve map editor; make shift clicking draw a line, ctrl-clicking 'negate' the placement, and make brush sizes.
* Remove SDL2, replace with ink runner?


[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-white.svg)](https://snapcraft.io/gladupe)
